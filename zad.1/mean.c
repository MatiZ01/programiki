#include "mean.h"
float mean(float *array, unsigned int element) {
    float average = 0.0f;
    int index = 0;
    for (index = 0; index < element; index++) {
        average += array[index];
    }
    average /= (float) element;
    return average;
}
