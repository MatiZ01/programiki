#include "zad1.h"

float median(float x[], int elem)
{
    float med = 0.0;
    float temp = 0.0;
    int i, j = 0;
	for (i = 0; i<elem-1; i++) //Sorting
    {
	    for (j=0; j<elem-1-i; j++)
	    {
			if (x[j] > x[j+1])
			{
				temp = x[j+1];
				x[j+1] = x[j];
				x[j] = temp;
			}
		}
    }

    if( elem %2 == 0) //If number of elements is even
    {
        med = x[elem/2];
    }
    else //If number of elements is uneven
    {
        med = x[(elem/2-1 + elem/2+1) / 2];
    }

    return med;

}