#include "zad1.h"
#include <math.h>

float sd(float x[], int elem, float avg)
{
    float wynik = 0.0;
    float war = 0.0;

    for (int i = 0; i < elem; i++) //Obliczenie wariancji...
    {
        war += pow((x[i] - avg), 2);
    }

    war /= (elem - 1); //dotad
    wynik = sqrt(war); //Odchylenie
    return wynik;
}
