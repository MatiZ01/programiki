#include <stdlib.h>
#include <stdio.h>
#include "zad1.h"

void readrec(FILE *file, float *p_tabx, float *p_taby, float *p_tabRHO);

int main()
{
    FILE *plik;

    float *tabx;
    float *taby;
    float *tabRHO;

    int size = 50;
    tabx = (float*)malloc(size * sizeof(float));
    taby = (float*)malloc(size * sizeof(float));
    tabRHO = (float*)malloc(size * sizeof(float));

    if ((plik = fopen("P0001_attr.rec", "a+")) == NULL)
        printf("Blad otwarcia pliku.\n");
    else
    {
        readrec(plik, tabx, taby, tabRHO);
        fprintf(plik, "Dane dla X:\nSrednia: %f\nMediana: %f\nOdchylenie standardowe: %f\n", mean(tabx, size), median(tabx, size), sd(tabx, size, mean(tabx, size)));
        fprintf(plik, "Dane dla Y:\nSrednia: %f\nMediana: %f\nOdchylenie standardowe: %f\n", mean(taby, size), median(taby, size), sd(taby, size, mean(taby, size)));
        fprintf(plik, "Dane dla RHO:\nSrednia: %f\nMediana: %f\nOdchylenie standardowe: %f\n", mean(tabRHO, size), median(tabRHO, size), sd(tabRHO, size, mean(tabRHO, size)));
    }

    printf("Dane dla X:\nSrednia: %f\n", mean(tabx, size));
    printf("Mediana: %f\n", median(tabx, size));
    printf("Odchylenie standardowe: %f\n", sd(tabx, size, mean(tabx, size)));

    printf("Dane dla Y:\nSrednia: %f\n", mean(taby, size));
    printf("Mediana: %f\n", median(taby, size));
    printf("Odchylenie standardowe: %f\n", sd(taby, size, mean(taby, size)));

    printf("Dane dla RHO:\nSrednia: %f\n", mean(tabRHO, size));
    printf("Mediana: %f\n", median(tabRHO, size));
    printf("Odchylenie standardowe: %f\n", sd(tabRHO, size, mean(tabRHO, size)));

    free(tabx);
    free(taby);
    free(tabRHO);

    fclose(plik);

    return 0;
}

void readrec(FILE *file, float *p_tabx, float *p_taby, float *p_tabRHO)
{
    char lp[4] = "";
    int i = 0;
    
    while(fscanf(file, "%s %f %f %f", lp, &p_tabx[i], &p_taby[i], &p_tabRHO[i])>0)
        {
            printf("%s %f %f %f \n", lp, p_tabx[i], p_taby[i], p_tabRHO[i]);
            i++;
        }
    
    return;
}