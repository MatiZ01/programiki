#include "zad2.h"

float zad2_mean(float x[], int elem)
{
    int i = 0;
    float avg = 0.0;

    for(i = 0; i < elem; i++)
    {
        avg += x[i];
    }

    avg /= elem;
    return avg;
}