#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "zad2.h"
#define n 50

struct data //For all data from file
{
    float X[n];
    float Y[n];
    float RHO[n];
    
};

struct statistic //For statistics
{
    float Mean[2];
    float Mediana[2];
    float Stan_deviation[2];
};

int main()
{
    struct data data_1;
    struct statistic statistic_1;
    FILE *plik;
    
    if((plik=fopen("P0001_attr.rec", "r"))==NULL) //Checking if file can be opened
    {
        printf("BŁĄD!");
    }
    else 
    {
        fscanf(plik, "%*[^\n]\n");
        int i =0;
        while (fscanf(plik,"%*f\t%f\t%f\t%f\n", &data_1.X[i], &data_1.Y[i], &data_1.RHO[i])==3) //Reading from file and saving in struct arrays
        {
            printf("Tablica nr.1:"); //Printing (to check if the values have been read)
            printf("%f\n", data_1.X[i]);
            printf("Tablica nr.2:");
            printf("%f\n", data_1.Y[i]);
            printf("Tablica nr.3:");
            printf("%f\n", data_1.RHO[i]);
            i++;
        }


    }
    fclose(plik);


    float mx=zad2_mean(data_1.X, n); //Calculating statistics for X, Y and RHO
    float my=zad2_mean(data_1.Y, n);
    float mrho=zad2_mean(data_1.RHO, n);
    
    float mex =  zad2_median(data_1.X, n);
    float mey = zad2_median(data_1.Y, n);
    float merho = zad2_median(data_1.RHO, n);

    float sdx=zad2_sd(data_1.X,n, mx);
    float sdy=zad2_sd(data_1.Y,n, my);
    float sdrho=zad2_sd(data_1.RHO,n, mrho);

    statistic_1.Mean[0]=mx; //Assigning statistic values to structures
    statistic_1.Mediana[0]=mex;
    statistic_1.Stan_deviation[0]=sdx;
    statistic_1.Mean[1]=my;
    statistic_1.Mediana[1]=mey;
    statistic_1.Stan_deviation[1]=sdy;
    statistic_1.Mean[2]=mrho;
    statistic_1.Mediana[2]=merho;
    statistic_1.Stan_deviation[2]=sdrho;

for(int i = 0; i<3; i++) //Printing statistics
{
    printf("Tablica statystyk %d to: %f\t%f\t%f\n", i+1 ,statistic_1.Mean[i], statistic_1.Mediana[i], statistic_1.Stan_deviation[i]);
}

if ((plik = fopen("P0001_attr.rec", "a+"))==NULL)
{
    printf("Blad otwarcia pliku");
}
else
{
    char *p;
    char *c = "----";
    rewind(plik);
    int finded = 0;
    char linia[10000] = "";
    while(fgets(linia, 10000, plik))
    {
        p = strstr(linia, c); //Checking if a line exists in a file
        if(p!=NULL)
        {
            finded = 1; //If already in the file, break the loop
            break;
        }
    }
        if (finded == 0) //If not, save to file
        {
            fprintf(plik,"\n-----------\n");
            fprintf(plik,"Wartosc sredniej dla X: %f",mx);
            fprintf(plik,"\nWartosc sredniej dla Y: %f", my);
            fprintf(plik,"\nWartosc sredniej dla RHO: %f", mrho);
            fprintf(plik,"\nMediana dla X : %f", mex);
            fprintf(plik,"\nMediana dla Y : %f", mey);
            fprintf(plik,"\nMediana dla RHO: %f", merho);
            fprintf(plik, "\nOdchylenie standardowe dla X: %f", sdx);
            fprintf(plik, "\nOdchylenie standardwe dla Y: %f", sdy);
            fprintf(plik, "\nOdchylenie standardowe dla RHO: %f", sdrho);
            fprintf(plik, "\n-----------");
        }
    }
    fclose(plik);
    return 0;
}