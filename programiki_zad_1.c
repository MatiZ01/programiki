#include <stdio.h>
#include <stdlib.h>
#include "mean.h"
#include "median.h"
#include "deviation.h"


int main() {
    FILE *file_data;
    if ((file_data = fopen("./P0001_attr.rec", "r")) == NULL) {
        printf("Error! opening file");
        // Program exits if the file pointer returns NULL.
        exit(1);
    }

    unsigned int arrays_elements_max = 50;

    //    calloc   - https://www.cplusplus.com/reference/cstdlib/calloc/
    float *X_array = (float *) calloc(arrays_elements_max, sizeof(float));
    float *Y_array = (float *) calloc(arrays_elements_max, sizeof(float));
    float *RHO_array = (float *) calloc(arrays_elements_max, sizeof(float));


    int character = EOF;
    int linecount = 0;
    while ((character = fgetc(file_data)) != EOF) {
        if (character == '\n') linecount++;
        if (character == '-') { break; }
        if (linecount != 0) {
            int lp = 0;
            unsigned int index = linecount - 1;
            fscanf(file_data, "%d. %f \t%f \t%f", &lp, &X_array[index], &Y_array[index], &RHO_array[index]);
        }
    }
    fclose(file_data);

    // Printing data from Arrays
    int print_index;
    for (print_index = 0; print_index < arrays_elements_max; ++print_index) {
        printf("X = %f \tY = %f \t RHO = %f\n", X_array[print_index], Y_array[print_index], RHO_array[print_index]);
    }


    printf("-----------------\n");
    printf("Srednia Tablicy X = %f\n", mean(X_array, arrays_elements_max));
    printf("Srednia Tablicy Y = %f\n", mean(Y_array, arrays_elements_max));
    printf("Srednia Tablicy RHO = %f\n\n", mean(RHO_array, arrays_elements_max));

    printf("Mediana Tablicy X = %f\n", median(X_array, arrays_elements_max));
    printf("Mediana Tablicy Y = %f\n", median(Y_array, arrays_elements_max));
    printf("Mediana Tablicy RHO = %f\n\n", median(RHO_array, arrays_elements_max));

    printf("Odchylenie Tablicy X = %f\n", deviation(X_array, arrays_elements_max));
    printf("Odchylenie Tablicy Y = %f\n", deviation(Y_array, arrays_elements_max));
    printf("Odchylenie Tablicy RHO = %f\n", deviation(RHO_array, arrays_elements_max));


    FILE *appendFile;
    appendFile = fopen("./P0001_attr.rec", "a");
    if (appendFile != NULL) {
        fprintf(appendFile, "-----------------\n");
        fprintf(appendFile, "Srednia Tablicy X = %f\n", mean(X_array, arrays_elements_max));
        fprintf(appendFile, "Srednia Tablicy Y = %f\n", mean(Y_array, arrays_elements_max));
        fprintf(appendFile, "Srednia Tablicy RHO = %f\n\n", mean(RHO_array, arrays_elements_max));

        fprintf(appendFile, "Mediana Tablicy X = %f\n", median(X_array, arrays_elements_max));
        fprintf(appendFile, "Mediana Tablicy Y = %f\n", median(Y_array, arrays_elements_max));
        fprintf(appendFile, "Mediana Tablicy RHO = %f\n\n", median(RHO_array, arrays_elements_max));

        fprintf(appendFile, "Odchylenie Tablicy X = %f\n", deviation(X_array, arrays_elements_max));
        fprintf(appendFile, "Odchylenie Tablicy Y = %f\n", deviation(Y_array, arrays_elements_max));
        fprintf(appendFile, "Odchylenie Tablicy RHO = %f\n", deviation(RHO_array, arrays_elements_max));
        fclose(appendFile);
    }
    fclose(appendFile);

    free(X_array);
    free(Y_array);
    free(RHO_array);
    return 0;
}
